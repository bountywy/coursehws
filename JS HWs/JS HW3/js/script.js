/*#1 Створити об'єкт "Документ", де визначити властивості "Заголовок, тіло, футер, дата". Створити об'єкт вкладений об'єкт - "Додаток". Створити об'єкт "Додаток", вкладені об'єкти, "Заголовок, тіло, футер, дата". Створити методи для заповнення та відображення документа.*/

let objDocument = {
	header: 'header data',
	body: 'body data',
	footer: 'footer data',
	date: 'date data',
	supplement: {
		header: 'header supplement data',
		body: 'body supplement data',
		footer: 'footer supplement data',
		date: 'date supplement data',
	},
	showDocument(objChoice) {
		for (let key in objChoice) {
			if (typeof objChoice[key] == 'string') {
				document.write(objChoice[key] + '<br>');
			}
		}
	},
}
document.write('<h1>Main Document</h1><br>');
objDocument.showDocument(objDocument);
document.write('<h2>Supplement for Document</h2><br>');
objDocument.showDocument(objDocument.supplement);

/*#2Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.*/

function map(fn, arr) {
	let newArr = [];
	for (let i = 0; i < arr.length; i++) {
		newArr.push(changeElement(arr[i]));
	}
	return newArr;
}
function changeElement(elem) {
	if (typeof elem === "number") {
		return -elem;
	} else if (typeof elem === "string") {
		let str = '';
		let lenStr = elem.length;
		for (let j = 0; j < lenStr; j++) {
			if (elem[j] === elem[j].toLowerCase()) {
				str += elem[j].toUpperCase();
			} else {
				str += elem[j].toLowerCase();
			}
		}
		return str;
	} else {
		return elem;
	}
}

console.log(map(changeElement, [1, 'bANK', 3, -4]));


