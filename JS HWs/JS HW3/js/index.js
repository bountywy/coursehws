/*Створіть сторінку коментарів та стилізуйте її як сторінка відгуків. 
За допомогою методів перебору виведіть на сторінку коментарі, пошту, номер коментаря та автора
Дані тут : https://jsonplaceholder.typicode.com/comments*/

//console.log(Array.isArray(comment));

function cutEmail(s) {
	let sLength = s.length;
	let author = '';
	for (i = 0; i < sLength; i++) {
		if (s[i] != '@') {
			author += s[i];
		} else {
			break;
		}
	}
	return author;
}

comment.forEach((a) => {
	const markup = `
	<div class="post">
		<div class="post-title">
			<div class="postid">#${a.postId} post</div>
			<div class="postname">${a.name}</div>
		</div>
		<div class="post-body">
			<div class="post-userdata">
				<div class="post-body-id">response #${a.id}</div>
				<div class="post-body-author">Author: ${cutEmail(a.email)}</div>
				<div class="post-body-email">Email: ${a.email}</div>
			</div>
			<div class="post-body-text">${a.body}</div>
		</div>
	</div>
	`
	document.getElementById('post').innerHTML += markup;

})