/*1. Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}".
Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.*/
class Phone {
	constructor(phone_number, phone_model, phone_weight) {
		this.number = phone_number;
		this.model = phone_model;
		this.weight = phone_weight;
	}
	receiveCall(name) {
		console.log(`Телефонує ${name}`);
	}
	getNumber() {
		console.log(this.number);
	}
}
//1st екземпляр класу Phone.
let iphone = new Phone('0992108317', 'iPhone 13 Pro', '150 g.');
console.log(iphone);
iphone.receiveCall('Jonh');
iphone.getNumber();
//2nd екземпляр класу Phone.
let samsung = new Phone('0992108318', 'Samsung Galaxy', '160 g.');
console.log(samsung);
samsung.receiveCall('David');
samsung.getNumber();
//3rd екземпляр класу Phone.
let redmi = new Phone('0992108319', 'Redmi Note 13', '180 g.');
console.log(redmi);
redmi.receiveCall('Alex');
redmi.getNumber();

/* Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
- Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].*/

function filterBy(arr, elem) {
	let newArr = [];
	if (elem == "undefined" || elem == "object" || elem == "boolean" || elem == "number" || elem == "bigint" || elem == "string" || elem == "symbol" || elem == "function") {
		for (i = 0; i < arr.length; i++) {
			if (typeof arr[i] != elem) {
				newArr.push(arr[i]);
			}
		}
		return newArr;
	} else {
		console.error('Ви ввели невірний тип даних. Доступні типи даних: "undefined", "object", "boolean", "number", "bigint", "string", "function"');
	}
}

console.log(filterBy(['hello', false, 'world', 23, '23', null, undefined, { name: 'John', age: 34 }, function () { }, [1, 2, 3, 4], true, 42n], 'object'));

