/*ДЗ 1
Використовуючи цикли намалюйте: 
1 Порожній прямокутник
Заповнений 
2 Рівносторонній трикутний 
3 Прямокутний трикутник 
4 Ромб*/

//1 Порожній прямокутник
for (let i = 1; i <= 10; ++i) {
	for (let j = 1; j <= 30; j++) {
		if (i == 1 || i == 10 || j == 1 || j == 30) {
			document.write('<span>');
			document.write('*');
			document.write('</span>');
		} else {
			document.write('<span>');
			document.write("&nbsp&nbsp")
			document.write('</span>');
		}
	}
	document.write('<br>');
}

document.write('<br>');
document.write('<hr>');
document.write('<br>');

//Рівносторонній трикутник 
for (let i = 0; i < 20; ++i) {
	for (let space = (20 - i); space > 0; space--) {
		document.write('<span>');
		document.write("")
		document.write('</span>');
	}
	for (let j = 0; j <= i; j++) {
		document.write('<span>');
		document.write('*');
		document.write('</span>');
	}

	document.write('<br>');
}
document.write('<br>');
document.write('<hr>');
document.write('<br>');

//Прямокутний трикутник 
for (let i = 0; i < 20; ++i) {
	for (let space = 0; space < 20; space++) {
		document.write('<span>');
		document.write("")
		document.write('</span>');
	}
	for (let j = 0; j <= i; j++) {
		document.write('<span>');
		document.write('*');
		document.write('</span>');
	}

	document.write('<br>');
}
document.write('<br>');
document.write('<hr>');
document.write('<br>');

// Ромб
for (let i = 0; i < 20; ++i) {
	for (let space = (20 - i); space > 0; space--) {
		document.write('<span>');
		document.write("")
		document.write('</span>');
	}
	for (let j = 0; j <= i; j++) {
		document.write('<span>');
		document.write('*');
		document.write('</span>');
	}

	document.write('<br>');
}
for (let i = 20; i > 0; --i) {
	for (let space = 20 - i; space > 0; space--) {
		document.write('<span>');
		document.write("")
		document.write('</span>');
	}
	for (let j = 0; j < i; j++) {
		document.write('<span>');
		document.write('*');
		document.write('</span>');
	}

	document.write('<br>');
}
document.write('<br>');
document.write('<hr>');
document.write('<br>');

/*ДЗ 2
Створіть масив styles з елементами «Джаз» та «Блюз». 
Додайте «Рок-н-рол» до кінця.
Замініть значення всередині на «Класика». Ваш код для пошуку значення всередині повинен працювати для масивів з будь-якою довжиною
Видаліть перший елемент масиву та покажіть його.
Вставте «Реп» та «Реггі» на початок масиву.*/
let stylesArray = ['Jazz', 'Blues',];
if (Array.isArray(stylesArray)) {
	stylesArray.push("Rock'n'Roll"); //Додаю «Рок-н-рол» до кінця.
	console.log(stylesArray);
} else {
	console.log('Is not Array');
}
let arrLenght = stylesArray.length;
//console.log(arrLenght);
let middleArrayElement;
//пошук значення всередині масиву з будь-якою довжиною
if (arrLenght % 2 !== 0) {
	middleArrayElement = Math.floor(arrLenght / 2);
	//console.log(middleElement);
} else {
	middleArrayElement = arrLenght / 2;
	//console.log(middleElement)
}
stylesArray.splice(middleArrayElement, 1, 'Classic'); //Заміняю значення всередині на «Класика».
//console.log(arrLenght);
console.log(stylesArray);
//Видаліть перший елемент масиву та покажіть його
let firstArrayElement = stylesArray.shift(0, 1);
console.log(firstArrayElement);
//Вставте «Реп» та «Реггі» на початок масиву
//1st way
//stylesArray.unshift('Rap', 'Reggi');
//console.log(stylesArray);
//2nd way
stylesArray.splice(0, 0, 'Rap', 'Reggi');
console.log(stylesArray);