// 184. 
//Сделайте объект с тремя функциями, каждая из которых будет принимать параметром массив с числами. Сделайте так, чтобы первая функция возвращала сумму элементов массива, вторая функция - сумму квадратов, а третья - сумму кубов.
let summ = {
	sum: function (arr) {
		let sum = 0;
		for (i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		return sum;
	},
	squareSum: function (arr) {
		let sum = 0;
		for (i = 0; i < arr.length; i++) {
			sum += Math.pow(arr[i], 2);
		}
		return sum;
	},
	cubeSum: function (arr) {
		let sum = 0;
		for (i = 0; i < arr.length; i++) {
			sum += Math.pow(arr[i], 3);
		}
		return sum;
	},
}
console.log(summ.sum([4, 2,]));
console.log(summ.squareSum([4, 2,]));
console.log(summ.cubeSum([4, 2,]));

//197. Рекурсия и многомерные структуры

//Дан многомерный объект произвольного уровня вложенности, например, такой: . С помощью рекурсии выведите все примитивные элементы этого объекта в консоль.

let diffObj = {
	a: 1,
	b: {
		c: 2,
		d: 3,
		e: 4
	},
	f: {
		g: 5,
		j: 6,
		k: {
			l: 7,
			m: {
				n: 8,
				o: 9
			}
		}
	}
}

function func(obj) {
	for (let key in obj) {
		if (typeof obj[key] == 'object') {
			func(obj[key]);
		} else {
			console.log(obj[key]);
		}
	}
}

func({ a: 1, b: { c: 2, d: 3, e: 4 }, f: { g: 5, j: 6, k: { l: 7, m: { n: 8, o: 9 } } } });

console.log('----------------------------------');
//2. Дан многомерный массив произвольного уровня вложенности, например, такой: [1, [2, 7, 8], [3, 4, [5, [6, 7]]]]
// Напишите код, который развернет наш многомерный массив в одномерный. Для приведенного выше массива это будет выглядеть вот так: [1, 2, 7, 8, 3, 4, 5, 6, 7]
function getBaseArr(multyArr) {
	let result = [];
	for (let i = 0; i < multyArr.length; i++) {
		if (typeof multyArr[i] == 'object') {
			result = result.concat(getBaseArr(multyArr[i]));
		} else {
			result.push(multyArr[i]);
		}
	}
	return result;
}

const testArray = [1, [2, 7, 8], [3, 4, [5, [6, 7]]]];
const result = getBaseArr(testArray);
console.log(result);

//3. Дан многомерный объект произвольного уровня вложенности, например, такой:... С помощью рекурсии найдите сумму элементов этого объекта.
// 3.1 obj
let multyObj = { a: 1, b: { c: 2, d: 3, e: 4 }, f: { g: 5, j: 6, k: { l: 7, m: { n: 8, o: 9 } } } };

function getSumObj(multyObj) {
	let sum = 0;
	for (let key in multyObj) {
		if (typeof multyObj[key] == 'object') {
			sum += getSumObj(multyObj[key]);
		} else {
			sum += multyObj[key];
		}
	}
	return sum;
}

console.log(getSumObj(multyObj));

//3.2 Array
function getSumArr(multyArr) {
	let resultSum = 0;
	for (let i = 0; i < multyArr.length; i++) {
		if (typeof multyArr[i] == 'object') {
			resultSum += getSumArr(multyArr[i]);
		} else {
			resultSum += multyArr[i];
		}
	}
	return resultSum;
}
const testArr = [1, [2, 7, 8], [3, 4, [5, [6, 7]]]];
const sum = getSumArr(testArr);
console.log(sum);

//4. Дан многомерный массив произвольного уровня вложенности, содержащий внутри себя строки, например, такой:

let testArr4 = ['a', ['b', 'c', 'd'], ['e', 'f', ['g', ['j', 'k']]]];
// С помощью рекурсии слейте элементы этого массива в одну строку:'abcdefgjk'

function getString(multyArr) {
	let newStr = '';
	for (let i = 0; i < multyArr.length; i++) {
		if (typeof multyArr[i] == 'object') {
			newStr += getString(multyArr[i]);
		} else {
			newStr += multyArr[i];
		}
	}
	return newStr;
}

const newStr = getString(testArr4);
console.log(newStr);


// 5. Дан многомерный массив произвольного уровня вложенности, например, такой:
let testArr5 = [1, [2, 7, 8], [3, 4], [5, [6, 7]]];
// Возведите все элементы-числа этого массива в квадрат.
function changeArr(arr) {
	for (let i = 0; i < arr.length; i++) {
		if (typeof arr[i] == 'object') {
			arr[i] = changeArr(arr[i]);
		} else {
			arr[i] = Math.pow(arr[i], 2);
		}
	}
	return arr;
}
//const newArr = changeArr(testArr5);
//console.log(newArr);
//5.1 звести все до одномірного масиву

function getOneArr(arr) {
	let newArr = [];
	for (let i = 0; i < arr.length; i++) {
		if (typeof arr[i] == 'object') {
			newArr = newArr.concat(getOneArr(arr[i]));
		} else {
			newArr.push(arr[i]);
		}
	}
	return newArr;
}

getArr = (arr) => getOneArr(changeArr(arr));

console.log(getArr(testArr5));