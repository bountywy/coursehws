console.log('==============198 Map(elem)===================');

// Дан массив с числами. Используя метод map извлеките из каждого элемента массива квадратный корень и запишите результат в новый массив.
let arr = [1, 9, 16, 8, 36, 49, 25, 64, 81];
let newArr = arr.map(function (elem) {
	return Math.sqrt(elem).toFixed(1);
})
console.log(newArr);

//Дан массив со строками. Используя метод map в конец значению каждого элемента массива добавьте символ '!'.
let arrStr = ['j1', 'k9', 'l16', 'd8', 'f36', 't49', 's25', 'l64', 'o81'];
let newArrStr = arrStr.map(function (elem) {
	return elem[0].toUpperCase() + elem.substring(1) + '!';
})

console.log(newArrStr);

//Дан массив со строками. Используя метод map переверните символы каждой строки в обратном порядке.
let newReverseStrArr = arrStr.map(function (elem) {
	return elem.split('').reverse().join('');
})

console.log(newReverseStrArr);

// Дан следующий массив:
let arr5 = ['123', '456', '789'];
//Используя метод map преобразуйте этот массив в следующий: let arr = [	[1, 2, 3],	[4, 5, 6],	[7, 8, 9]];
let resultArr = arr5.map(function (params) {
	return params.split('');
})

console.log(resultArr);
console.log('============198 Map(elem, index)=================');

// Дан массив с числами. Используя метод map запишите в каждый элемент массива значение этого элемента, умноженное на его порядковый номер в массиве.

let newNumArr = arr.map(function (elem, index) {
	return elem * index;
})
console.log(newNumArr);

console.log('============200 filter(elem, index)=================');

//Дан массив с числами. Оставьте в нем только положительные числа.
let arrAll = [1, -9, -16, 18, 3, -49, 5, 64, -81, -85,];
console.log(arrAll.filter(elem => elem > 0));
//Дан массив с числами. Оставьте в нем только отрицательные числа.
console.log(arrAll.filter(elem => elem < 0));
//Дан массив с числами. Оставьте в нем только числа, которые больше нуля, но меньше 10.
console.log(arrAll.filter(elem => elem < 10 && elem > 0));
//Дан массив со строками. Оставьте в нем только те строки, длина которых больше 5-ти символов.
let arrStrAll = ['index1', 'koodo9', 'loh16', 'd8', 'index36', 't49', 's25', 'l64', 'o81'];
console.log(arrStrAll.filter(elem => elem.length > 5));
//Дан массив с числами. Оставьте в нем только те числа, произведение которых на их порядковый номер меньше 30.
let productArr = arrAll.filter(function (params, index) {
	return params * index < 30;
})

console.log(productArr);

console.log('============201 every(elem, index)=================');
// Дан массив с числами. Проверьте то, что все элементы в массиве больше нуля.
let checkArr = newNumArr.every(function (params) {
	return params > 0;
})

console.log(checkArr);