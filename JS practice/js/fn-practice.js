//171 Практика на функции
//1.Сделайте функцию, которая параметром будет принимать массив и возвращать сумму его элементов.

function calcSum(arr) {
	let sum = 0;
	for (let i = 0; i < arr.length; i++) {
		sum += arr[i];
	}
	return sum;
}
console.log(calcSum([4, 0, 4, 3, 3, 6,]));

//2. Сделайте функцию, которая параметром будет принимать число и возвращать массив его делителей.
function getDividers(num) {
	let arr = [];
	let i = 1;
	for (; i <= num; i++) {
		if (num % i == 0) {
			arr.push(i);
		}
	}
	return arr;
}
console.log(getDividers(108));

//3. Сделайте функцию, которая параметром будет принимать строку и возвращать массив ее символов.
getSymbols = str => str.split('');
console.log(getSymbols('cc108xx'));

// 4.Сделайте функцию, которая параметром будет принимать строку и переворачивать ее символы в обратном порядке.
getReverseSymbols = str => str.split('').reverse().join('');
console.log(getReverseSymbols('cc108xx'));

//5.Сделайте функцию, которая параметром будет принимать строку и делать заглавной первую букву этой строки.
changeFirstLetter = str => str[0].toUpperCase() + str.substr(1);
console.log(changeFirstLetter('cc108xx'));

//6. Сделайте функцию, которая параметром будет принимать строку и делать заглавной первую букву каждого слова этой строки.
function changeWord(str) {
	let newStrArr = str.split(' ');
	for (let i = 0; i < newStrArr.length; i++) {
		newStrArr[i] = newStrArr[i].substr(0, 1).toUpperCase() + newStrArr[i].substr(1);
	}
	return newStrArr.join(' ');
}
console.log(changeWord('cc108xx x1o1 df56 u90tgg'));

//7. Сделайте функцию, заполняющую массив целыми числами от 1 до заданного.
function getIntegerNums(num) {
	let i = 1;
	let arr = [];
	while (i <= num) {
		arr.push(i);
		i++;
	}
	return arr;
}
console.log(getIntegerNums(15));

//8. Сделайте функцию, которая будет возвращать случайный элемент из массива.
getRandomElement = arr => arr[Math.floor(Math.random() * arr.length)];

console.log(getRandomElement(['a', 'g', 5, 'h', 't', 6,]));

//9. Сделайте функцию, которая параметром будет принимать число и проверять, простое оно или нет.
function isPrimeNum(num) {
	let i = 2;
	let count = 0;
	for (; i <= num; i++) {
		if (num % i == 0) {
			count++;
		}
	}
	if (count > 1) {
		return false;
	}
	return true;
}
console.log(isPrimeNum(4));

//10. Сделайте функцию, которая будет проверять пару чисел на дружественность. Дружественные числа - два числа, для которых сумма всех собственных делителей первого числа равна второму числу и наоборот, сумма всех собственных делителей второго числа равна первому числу.
isFriendlyNum = (a, b) => getDividersSum(a) == b && getDividersSum(b) == a;
function getDividersSum(num) {
	let j = 1;
	let sum = 0;
	for (; j < num; j++) {
		if (num % j == 0) {
			sum += j;
		}
	}
	return sum;
}

console.log(isFriendlyNum(220, 284));

//11. Используя созданную вами функцию из предыдущей задачи найдите все пары дружественных чисел в промежутке от 1 до 1000.
/*function getFriendly(min, max) {
	let result = [];
	let sum1, sum2;
	for (let i = min; i < max; i++) {
		sum1 = getDivSum(i);
		for (let k = i + 1; k <= max; k++) {
			sum2 = getDivSum(k);
			if (sum1 == k && sum2 == i) {
				result.push([sum1, sum2]);
			}
		}
	}
	return result;
}
function getDivSum(num) {
	let j = 1;
	let sum = 0;
	for (; j < num; j++) {
		if (num % j == 0) {
			sum += j;
		}
	}
	return sum;
}
console.log('Frendly Numbers: ' + getFriendly(1, 1000));*/

//12.Сделайте функцию, которая будет проверять число на совершенность. Совершенное число - это число, сумма собственных делителей которого равна этому числу.

function isPerfectNumber(num) {
	return num == getOwnDivSum(num);
}

function getOwnDivSum(num) {
	let sum = 0;
	for (let j = 1; j < num; j++) {
		if (num % j == 0) {
			sum += j;
		}
	}
	return sum;
}
console.log(isPerfectNumber(28));

//13.Найдите все счастливые билеты. Счастливый билет - это билет, в котором сумма первых трех цифр его номера равна сумме вторых трех цифр его номера.
function isLuckyTicket(num) {
	if (getString(num).length % 2 == 0) {
		return checkSum(num);
	}
};

getString = num => String(num);

function checkSum(num) {
	let str1, str2;
	let halfLength = getString(num).length / 2;
	let str = getString(num);
	str1 = str.substr(0, halfLength);
	str2 = str.substr(halfLength);
	return getHalfNumSum(str1) == getHalfNumSum(str2);
}

function getHalfNumSum(num) {
	let sumHalfNum = 0;
	let strArr = getString(num);
	for (let i = 0; i < strArr.length; i++) {
		sumHalfNum += +strArr[i];
	}
	return sumHalfNum;
}

console.log(isLuckyTicket(280002100370));

//14.Сделайте функцию, которая параметром будет принимать два числа и возвращать массив их общих делителей.
function getDividersArr(num) {
	let arr = [];
	for (let y = 2; y <= num; y++) {
		if (num % y == 0) {
			arr.push(y);
		}
	}
	return arr;
}

checkEmptyDivArr = (arr) => arr.length != 0 ? arr : 'Общих делителей не найдено';

function getCommonArr(num1, num2) {
	let arr1 = getDividersArr(num1);
	let arr2 = getDividersArr(num2);
	let divArr = [];
	for (let f = 0; f < arr1.length; f++) {
		for (let s = 0; s < arr2.length; s++) {
			if (arr1[f] == arr2[s]) {
				divArr.push(arr1[f]);
			}
		}
	}
	return checkEmptyDivArr(divArr);
}
console.log(getCommonArr(10, 100));

//15. Сделайте функцию, которая будет принимать строку на укр языке, а возвращать ее транслит.
function transcription(str) {
	let answer = '';
	const converter = {
		'а': 'a', 'б': 'b', 'в': 'v', 'г': 'h', 'д': 'd',
		'е': 'e', 'є': 'ie', 'ж': 'zh', 'з': 'z', 'и': 'y',
		'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
		'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't',
		'у': 'u', 'ф': 'f', 'х': 'kh', 'ц': 'ts', 'ч': 'ch',
		'ш': 'sh', 'щ': 'sch', 'ь': '', 'ї': 'i', 'э': 'e',
		'ю': 'iu', 'я': 'ia', 'і': 'i',

		'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'H', 'Д': 'D',
		'Е': 'E', 'Є': 'Ye', 'Ж': 'Zh', 'З': 'Z', 'И': 'Y',
		'Й': 'Y', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N',
		'О': 'O', 'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T',
		'У': 'U', 'Ф': 'F', 'Х': 'Kh', 'Ц': 'Ts', 'Ч': 'Ch',
		'Ш': 'Sh', 'Щ': 'Sch', 'Ь': '', 'Ї': 'Yi', 'Э': 'E',
		'Ю': 'Yu', 'Я': 'Ya', 'І': 'I',
	};
	for (let w = 0; w < str.length; w++) {
		console.log(converter[str[w]]);
		if (converter[str[w]] === undefined) {
			answer += str[w];
		} else {
			answer += converter[str[w]];
		}
	}
	return answer;
}

console.log(transcription('Зробіть функцію, яка прийматиме рядок на українській мові, а повертатиме її трансліт'));

//16. Сделайте функцию, которая будет принимать число, а возвращать это число прописью. Пусть функция работает с числами до 999.

function getСapitalUnitNumber(num) {
	const unitArr = ['один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'];
	return unitArr[num - 1];
}
function getСapitalTensNumber(num) {
	const tenArr = ['одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать'];
	return tenArr[num - 11];
}
function getСapitalDozenNumber(num) {
	const dozensArr = ['десять', 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'];
	let dozens = num % 10; // 0 & 1..9
	let index = (num - dozens) / 10; // 22 - 2  
	if (dozens == 0) {
		return dozensArr[index - 1];
	} else {
		return dozensArr[index - 1] + ' ' + getСapitalUnitNumber(dozens);
	}
}
function getСapitalHundredsNumber(num) {
	const hundredsArr = ['сто', 'двісті', 'триста', 'чотириста', 'пятсот', 'шістсот', 'сімсот', 'вісімсот', 'девятсот'];
	let hundreds = num % 100; // якщо цілі сотні без остатка. 111->11
	let index = (num - hundreds) / 100; // 221 - 21 
	if (hundreds == 0) {
		return hundredsArr[index - 1];
	} else if (hundreds >= 1 && hundreds <= 9) {
		return hundredsArr[index - 1] + ' ' + getСapitalUnitNumber(hundreds);
	} else if (hundreds >= 11 && hundreds <= 19) {
		return hundredsArr[index - 1] + ' ' + getСapitalTensNumber(hundreds);
	} else {
		return hundredsArr[index - 1] + ' ' + getСapitalDozenNumber(hundreds);
	}
}

function getСapitalNumber(num) {
	if (num >= 1 && num <= 9) {
		return getСapitalUnitNumber(num);
	} else if (num >= 11 && num <= 19) {
		return getСapitalTensNumber(num);
	} else if (num == 10 || (num >= 20 && num <= 99)) {
		return getСapitalDozenNumber(num);
	} else if (num >= 100 && num <= 999) {
		return getСapitalHundredsNumber(num);
	} else {
		return console.error('Number(more than 999) or mistake with type of data')
	}
}
console.log(getСapitalNumber('305'));

function func() {
	return '!';
}

console.log(func); // увидим код функции