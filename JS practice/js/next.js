//Перебор многомерных объектов
//С помощью вложенных циклов выведите на экран все строки с данными.
let data = {
	1: [
		'data11',
		'data12',
		'data13',
	],
	2: [
		'data21',
		'data22',
		'data23',
	],
	3: [
		'data31',
		'data32',
		'data33',
	],
	4: [
		'data41',
		'data42',
		'data43',
	],
};

for (let key in data) {
	for (let datakey of data[key]) {
		console.log(datakey);
	}
}

let dataTwo = [
	{
		1: 'data11',
		2: 'data12',
		3: 'data13',
	},
	{
		1: 'data21',
		2: 'data22',
		3: 'data33',
	},
	{
		1: 'data31',
		2: 'data32',
		3: 'data33',
	},
];

for (let i of dataTwo) {
	for (let keyI in i) {
		console.log(i[keyI]);
	}
}
//С помощью вложенных циклов выведите на экран все строки с данными.
let dataThree = [
	{
		1: [
			'data111',
			'data112',
			'data113',
		],
		2: [
			'data121',
			'data122',
			'data123',
		],
	},
	{
		1: [
			'data211',
			'data212',
			'data213',
		],
		2: [
			'data221',
			'data222',
			'data223',
		],
	},
	{
		1: [
			'data411',
			'data412',
			'data413',
		],
		2: [
			'data421',
			'data422',
			'data423',
		],
	},
];

for (let index of dataThree) {
	for (let key in index) {
		for (let element of index[key]) {
			console.log(element);
		}
	}
}

//Дан следующий массив работников. Выведите на экран данные каждого работника в формате имя - зарплата.
let employees = [
	{
		name: 'name1',
		salary: 300,
	},
	{
		name: 'name2',
		salary: 400,
	},
	{
		name: 'name3',
		salary: 500,
	},
];
for (let index of employees) {
	//console.log(index);
	let user = `${index.name} have ${index.salary}$`;
	console.log(user);
}
// 147 Выведите на экран сумму зарплат тех работников, возраст которых равен или более 30 лет.
let employ = [
	{
		name: 'name1',
		salary: 300,
		age: 28,
	},
	{
		name: 'name2',
		salary: 400,
		age: 29,
	},
	{
		name: 'name3',
		salary: 500,
		age: 30,
	},
	{
		name: 'name4',
		salary: 600,
		age: 31,
	},
	{
		name: 'name5',
		salary: 700,
		age: 32,
	},
];
let sunEmployess = 0;
for (let index of employ) {
	if (index.age >= 30) {
		sunEmployess += index.salary;
	}
}
console.log(sunEmployess);

let affairs = {
	'2019-12-28': ['data11', 'data12', 'data13'],
	'2019-12-29': ['data21', 'data22', 'data23'],
	'2019-12-30': ['data31', 'data32', 'data33'],
}
//Добавьте еще одно дело в дату '2019-12-29'.
affairs['2019-12-29'].push('data24');
//Добавьте еще два дела в дату '2019-12-31'.
affairs['2019-12-31'] = [];
affairs['2019-12-31'].push('data41', 'data42')

for (let key in affairs) {
	console.log(affairs[key]);
}
//150
let students = {
	'group1': {
		'subgroup11': ['student111', 'student112', 'student113'],
		'subgroup12': ['student121', 'student122', 'student123'],
	},
	'group2': {
		'subgroup21': ['student211', 'student212', 'student213'],
		'subgroup22': ['student221', 'student222', 'student223'],
	},
	'group3': {
		'subgroup31': ['student311', 'student312', 'student313'],
		'subgroup32': ['student321', 'student322', 'student323'],
	},
};

//Добавьте нового студента в подгруппу 'subgroup11'.

students.group1.subgroup11.push('student114');
//Добавьте в первую группу еще одну подгруппу.
students.group1.subgroup13 = [];
students.group1.subgroup13.push('student131', 'student132');
//Сделайте четвертую группу, в ней сделайте подгруппу и добавьте в нее двух новых студентов.
students.group4 = {};
students.group4.subgroup41 = [];
students.group4.subgroup41.push('student411', 'student412');

for (let key in students) {
	let subStudents = students[key];
	console.log(key);
	for (let subKey in subStudents) {
		console.log(subKey);
		console.dir(subStudents[subKey]);
	}
}

//152 Строковые методы
// 1. Работа с регистром символов (toUpperCase toLowerCase)
// 2.Работа с substr, substring, slice
let str = 'abcde.html';
let sub = str.slice(1, -1);

console.log(sub);
//Работа с indexOf
//Дана строка 'abcde'. Определите позицию буквы 'c' в этой строке.
console.log(str.indexOf('c'));
//Дана строка. Проверьте, есть ли в этой строке символ 'a'.
console.log(str.indexOf('a'));
//Дана строка. Проверьте, начинается ли эта строка с символа 'a'.
console.log(str.indexOf('a', 0));
//Дана строка. Проверьте, заканчивается ли эта строка на символ 'a'.
console.log(str.indexOf('a', str.length - 1));
//Дана строка. Проверьте, начинается ли эта строка на 'http://'.
console.log(str.indexOf('http://', 0));
//Дана строка. Проверьте, заканчивается ли эта строка на '.html'.
console.log(str.lastIndexOf('.html', str.length - 1))

//Дана строка 'я учу javascript!'. Вырежьте из нее слово 'учу' и слово 'javascript' тремя разными способами (через substr, substring, slice).
let string = 'я учу javascript!';
console.log(string.length);
let js = 'javascript';
let learn = 'учу';
//substr
let newstring = string.substr(string.indexOf(learn), learn.length);
let newString = string.substr(string.indexOf(js), js.length);
console.log(newstring);
console.log(newString);
//substring
let newstr = string.substring(string.indexOf(learn), string.indexOf(learn) + learn.length);
let newStr = string.substring(string.indexOf(js), string.indexOf(js) + js.length);
console.log(newstr);
console.log(newStr);
//slice
let newSlstr = string.slice(string.indexOf(learn), string.indexOf(learn) + learn.length);
let newSlStr = string.slice(string.indexOf(js), string.indexOf(js) + js.length);
console.log(newSlstr);
console.log(newSlStr);

//Практика на использования изученных методов
//Преобразуйте последнюю букву строки в верхний регистр.
let str1 = 'London is the capital city of the United Kingdom';
let res1 = str1.slice(0, -1) + str1[str1.length - 1].toLocaleUpperCase();
console.log(res1);
//Преобразуйте первые 2 буквы строки в верхний регистр.
let res2 = str1.substr(0, 2).toUpperCase() + str1.slice(2);
console.log(res2);
//Преобразуйте первую букву строки в нижний регистр.
let res3 = str1.substring(0, 1).toLocaleLowerCase() + str1.slice(1);
console.log(res3);
//код, который преобразует первую букву каждого слова в верхний регистр
let word = str1.split(' ');
for (k = 0; k < word.length; k++) {
	word[k] = word[k].slice(0, 1).toUpperCase() + word[k].slice(1);
}
let res4 = word.join(' ');
console.log(res4);

//Преобразуйте строку 'var_test_text' в 'VarTestText'. Написанный код должен работать для любых строк такого типа (то есть для строк, в которых слова разделены символов подчеркивания).
//Модифицируйте предыдущую задачу так, чтобы первая буква новой строки была в нижнем регистре.

let text = 'var_test_text';
let textArr = text.split('_');
for (let i = 0; i < textArr.length; i++) {
	textArr[i] = textArr[i].slice(0, 1).toLowerCase() + textArr[i].slice(1).toLocaleUpperCase();
}
let res5 = textArr.join('');
console.log(res5);

//163 Применение return в циклах

//Напишите функцию, которая параметром будет принимать число и делить его на 2 столько раз, пока результат не станет меньше 10. Пусть функция возвращает количество итераций, которое потребовалось для достижения результата.
function divideTwo(num) {
	let i = 0;
	while (num > 10) { // бесконечный цикл
		num = num / 2;
		i++;
	}
	return i;
}

console.log(divideTwo(200));

function func(num) {
	let i = 0;
	for (; num > 10; i++) {
		num /= 2;
	}

	return i;
}
console.log(func(200));

//167 Флаги в функциях
//1. Сделайте функцию, которая параметром будет принимать массив с числами, и проверять, что все элементы в этом массиве являются четными числами.
function evenNum(arr) {
	for (let i = 0; i < arr.length; i++) {
		if (arr[i] % 2 != 0) {
			return false;
		}
	}
	return true;
}

console.log(evenNum([4, 2, 6, 4, 6, 6,]));
//2. Сделайте функцию, которая параметром будет принимать число и проверять, что все цифры это числа являются нечетными.
function oddNum(num) {
	let strNum = num.toString();
	for (let i = 0; i < strNum.length; i++) {
		if (strNum[i] % 2 == 0) {
			return false;
		}
	}
	return true;
}
console.log(oddNum(557));
//3. Сделайте функцию, которая параметром будет принимать массив и проверять, есть ли в этом массиве два одинаковых элемента подряд.
function doubleNum(arr) {
	for (let i = 0; i < arr.length; i++) {
		if (arr[i] == arr[i + 1]) {
			return true;
		}
	}
	return false;
}
console.log(doubleNum([4, 2, 4, 3, 3, 6,]));

//168. Логические операторы без if в функциях
console.log(isPrime(14)); // должен вывести true

function isPrime(num) {
	for (let i = 2; i < num; i++) {
		if (num % i !== 0) {
			return true;
		} else {
			return false;
		}
	}
}

